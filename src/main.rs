// 导入必要的库和模块
use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use std::collections::HashMap;
use rand::prelude::*;

// 定义 Lambda 函数接收的请求载荷结构体
#[derive(Deserialize)]
struct Request {
    id: Option<String>,
}

// 定义 Lambda 函数返回的响应载荷结构体
#[derive(Serialize)]
struct Response {
    name: Option<String>,
    gender: Option<String>,
    height: Option<String>,
    weight: Option<String>,
}

// Lambda 函数的主入口点，使用 `tokio` 运行时
#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

// 负责处理 Lambda 事件的处理函数
async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    let request: Request = serde_json::from_value(event.payload)?;

    let config = load_from_env().await;
    let client = Client::new(&config);

    let response = search_people(&client, request.id).await?;

    Ok(json!(response))
}

// 根据提供的参数在 DynamoDB 中搜索人员信息的函数
async fn search_people(client: &Client, id: Option<String>) -> Result<Response, LambdaError> {
    let table_name = "people_information";

    if let Some(id_val) = id {
        let result = client.get_item()
            .table_name(table_name)
            .key("id", AttributeValue::S(id_val))
            .send()
            .await?;

        if let Some(item) = result.item {
            let name = item.get("name").and_then(|val| val.as_s().ok()).map(|s| s.to_string());
            let gender = item.get("gender").and_then(|val| val.as_s().ok()).map(|s| s.to_string());
            let height = item.get("height").and_then(|val| val.as_s().ok()).map(|s| s.to_string());
            let weight = item.get("weight").and_then(|val| val.as_s().ok()).map(|s| s.to_string());

            Ok(Response {
                name,
                gender,
                height,
                weight,
            })
        } else {
            Err(LambdaError::from("No matching person found"))
        }
    } else {
        Err(LambdaError::from("ID is required"))
    }
}
