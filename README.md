# IDS721_mini_project5
## Overview
We're embarking on a project to create a lightweight, serverless microservice using Rust, designed specifically for AWS Lambda. Our aim is to simplify database integration and harness the power of Rust for efficient serverless operations. Through this endeavor, we'll focus on making the service easy to use while ensuring it's powerful enough to handle our needs. We'll document every step to make everything clear and provide visual proofs of our success with screenshots and in-depth explanations of how everything works together.
## Steps：
1. Use cargo lambda new week5_function to create a new lambda folder.<br>
2. Write the function in main.rs file under src folder, the main function of my function is to recognize the primary key id first, and then extract the information under each tag under the primary key id, such as: gender, name, height, weight, etc., and then output it. The code is shown below.<br>
![Alt text](image-15.png)
![Alt text](image-14.png)
3. Configure the cargo.toml file as follows.<br>
![Alt text](image-13.png)
4. Add role permissions as shown below:<br>
![Alt text](image-12.png)
5. Deploy the lambda to the AWS cloud server with cargo build --release and cargo Lambda deploy--regionus-east-1--iam-role arn:aws:iam::905418421277:role/IDS721<br>
6. In Amazon's DynamoDB, create a new database called people_information, which is shown below:<br>
![Alt text](image-11.png) 
7. Under the lambda function, create an APIGateway trigger which is of type REST API<br>
![Alt text](image-10.png)
8. Tap inside the API, create a resource, add the post method to the resource, and enable CORS. Check the Post as a method in the CORS interface, and click Save.<br>
![Alt text](image-9.png)
9. Then click Redeploy in the upper right corner and create a new stage to deploy the API to the new stage.<br>
![Alt text](image-8.png)
10. Find the stage in the left column of the APIGateway service interface, then find the newly created resource column of the latest created stage, and then find the corresponding URL link in the post column.<br>
![Alt text](image-7.png)



## Display:
#### Lambda function:
 ![Alt text](image-6.png)
#### Lambda function triggers:
These are the API gateway triggers attached to the lambda function.<br>
 ![Alt text](image-5.png)
#### Lambda function test:
##### Test data:
 ![Alt text](image-4.png)
##### Test result:
![Alt text](image-3.png)
#### DynamoDB Usage:
This is the table named people_information created in AWS DynamoDB, and some sample data is in the table.<br>
![Alt text](image-2.png)
 
#### Service Implementation:
To send a POST request in my specific case, I need to use a particular URL in my case is:<br>
https://dounziuyw5.execute-api.us-east-1.amazonaws.com/test/week5_function/resource<br>
We can test the service's functionality using Postman to send POST requests. We can use Postman to send POST requests to test the service's functionality. We search for an employee using the id parameter and receive their name, gender, height, and weight as the response.<br>
![Alt text](image.png)
 
![Alt text](image-1.png)

